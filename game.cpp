#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cstdlib>
#include <ctime>

class Item {
private:
    std::string name;
    std::string description;
public:
    Item(const std::string& name, const std::string& desc);
    std::string GetName() const;
    std::string GetDescription() const;
    void Interact();
};

Item::Item(const std::string& name, const std::string& desc) : name(name), description(desc) {}

std::string Item::GetName() const {
    return name;
    }

    std::string Item::GetDescription() const {
        return description;
    }

    void Item::Interact() {
        std::cout << "You interact with the " << name << "." << std::endl;
        // Add your interaction logic here
    }

    class Room {
    private:
        std::string description;
        std::map<std::string, Room*> exits;
        std::vector<Item> items;
    public:
        Room(const std::string& desc);
        std::string GetDescription() const;
        std::vector<Item>& GetItems();
        std::map<std::string, Room*>& GetExits();
        Room* GetExit(const std::string& direction);
        void AddExit(const std::string& direction, Room* room);
        void AddItem(const Item& item);
        void RemoveItem(const Item& item);
    };

    Room::Room(const std::string& desc) : description(desc) {}

    std::string Room::GetDescription() const {
        return description;
    }

    std::vector<Item>& Room::GetItems() {
        return items;
    }

    std::map<std::string, Room*>& Room::GetExits() {
        return exits;
            }

            Room* Room::GetExit(const std::string& direction) {
                auto it = exits.find(direction);
                if (it != exits.end()) {
                    return it->second;
                }
                return nullptr;
            }

            void Room::AddExit(const std::string& direction, Room* room) {
                exits[direction] = room;
            }

            void Room::AddItem(const Item& item) {
                items.push_back(item);
            }

            void Room::RemoveItem(const Item& item) {
                for (auto it = items.begin(); it != items.end(); ++it) {
                    if (it->GetName() == item.GetName()) {
                        items.erase(it);
                        break;
                    }
                }
            }

            class Character {
            private:
                std::string name;
                int health;
                std::vector<Item> inventory;
            public:
                Character(const std::string& name, int health);
                void TakeDamage(int damage);
                void AddItem(const Item& item);
                void RemoveItem(const Item& item);
                std::vector<Item>& GetInventory();
            };

            Character::Character(const std::string& name, int health) : name(name), health(health) {}

            void Character::TakeDamage(int damage) {
                health -= damage;
                if (health <= 0) {
                    std::cout << name << " has been defeated." << std::endl;
                    // Add your defeat logic here
                }
            }

            void Character::AddItem(const Item& item) {
                inventory.push_back(item);
            }

            void Character::RemoveItem(const Item& item) {
                for (auto it = inventory.begin(); it != inventory.end(); ++it) {
                    if (it->GetName() == item.GetName()) {
                        inventory.erase(it);
                        break;
                    }
                }
            }

            std::vector<Item>& Character::GetInventory() {
                return inventory;
            }

            class Player : public Character {
            private:
                Room* location;
            public:
                Player(const std::string& name, int health);
                Room* GetLocation();
                void SetLocation(Room* room);
            };

            Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

            Room* Player::GetLocation() {
                return location;
            }

            void Player::SetLocation(Room* room) {
                location = room;
            }

            // Function to play rock-paper-scissors game
            bool PlayRockPaperScissors() {
                std::cout << "Let's play Rock-Paper-Scissors!" << std::endl;
                std::cout << "Choose your move (rock, paper, scissors): ";
                std::string playerMove;
                std::cin >> playerMove;

                // Generate random move for the monster
                std::string monsterMove;
                srand(time(0));
                int randomNum = rand() % 3;
                if (randomNum == 0) {
                    monsterMove = "rock";
                } else if (randomNum == 1) {
                    monsterMove = "paper";
                } else {
                    monsterMove = "scissors";
                }

                std::cout << "Monster chose: " << monsterMove << std::endl;

                // Determine the winner
                if (playerMove == monsterMove) {
                    std::cout << "It's a tie!" << std::endl;
                    return false;
                } else if ((playerMove == "rock" && monsterMove == "scissors") ||
                           (playerMove == "paper" && monsterMove == "rock") ||
                           (playerMove == "scissors" && monsterMove == "paper")) {
                    std::cout << "You win!" << std::endl;
                    return true;
                } else {
                    std::cout << "You lose!" << std::endl;
                    return false;
                }
            }

            int main() {
                // Create Rooms
                Room startRoom("You are in a dimly lit room.");
                Room hallway("You are in a long hallway.");
                Room treasureRoom("You have entered a treasure room!");
                Room kitchen("You are in a cozy kitchen.");
                Room garden("You are in a beautiful garden.");
                Room endingRoom("Congratulations! You have won the game!");

                // Define exits between rooms
                startRoom.AddExit("north", &hallway);
                hallway.AddExit("south", &startRoom);
                hallway.AddExit("north", &treasureRoom);
                treasureRoom.AddExit("south", &hallway);
                startRoom.AddExit("east", &kitchen);
                kitchen.AddExit("west", &startRoom);
                kitchen.AddExit("north", &garden);
                garden.AddExit("south", &kitchen);
                garden.AddExit("east", &endingRoom);

                // Create Items
                Item key("Key", "A shiny key that looks important.");
                Item sword("Sword", "A sharp sword with a golden hilt.");
                Item apple("Apple", "A juicy red apple.");
                Item book("Book", "An old book with a worn-out cover.");
                Item potion("Potion", "A healing potion.");

                // Add items to rooms
                startRoom.AddItem(key);
                treasureRoom.AddItem(sword);
                kitchen.AddItem(apple);
                garden.AddItem(book);

                // Create a Player
                Player player("Alice", 100);

                // Set the player's starting location
                player.SetLocation(&startRoom);

                // Display initial location and items
                std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
                std::cout << "Items in the room:" << std::endl;
                for (const Item& item : player.GetLocation()->GetItems()) {
                    std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
                }

                // Game loop
                while (true) {
                    // Display available exits
                    std::cout << "Available Exits:" << std::endl;
                    for (const auto& exit : player.GetLocation()->GetExits()) {
                        std::cout << "- " << exit.first << std::endl;
                    }
                    std::cout << "- search" << std::endl;
                    std::cout << "- exit" << std::endl;

                    // Prompt for user input
                    std::string action;
                    std::cout << "Enter your action: ";
                    std::cin >> action;

                    if (action == "exit") {
                        std::cout << "Exiting the game..." << std::endl;
                        break;
                    } else if (action == "search" && player.GetLocation()->GetItems().size() > 0) {
                        // Look around for items
                        std::cout << "You found some items in the room:" << std::endl;
                        for (const Item& item : player.GetLocation()->GetItems()) {
                            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
                        }
                    } else if (player.GetLocation()->GetExits().count(action) > 0) {
                        // Move to the selected room
                        Room* nextRoom = player.GetLocation()->GetExit(action);
                        player.SetLocation(nextRoom);
                        std::cout << "You have moved to " << player.GetLocation()->GetDescription() << std::endl;
                        if (player.GetLocation() == &endingRoom) {
                            std::cout << "Congratulations! You have won the game!" << std::endl;
                            break;
                        }
                    } else if (action == "interact") {
                        // Prompt for interaction
                        std::string itemName;
                        std::cout << "Enter the name of the item to interact with (or 'none' to skip): ";
                        std::cin >> itemName;

                        if (itemName != "none") {
                            // Find the item in the room
                            Item* itemToInteract = nullptr;
                            for (Item& item : player.GetLocation()->GetItems()) {
                                if (item.GetName() == itemName) {
                                    itemToInteract = &item;
                                    break;
                                }
                            }

                            if (itemToInteract != nullptr) {
                                // Interact with the item
                                itemToInteract->Interact();
                                // Add the item to the player's inventory
                                player.AddItem(*itemToInteract);
                                // Remove the item from the room
                                player.GetLocation()->RemoveItem(*itemToInteract);
                            } else {
                                std::cout << "Item not found. Please try again." << std::endl;
                            }
                        }
                    } else if (action == "look") {
                        std::cout << "There are no items in the room." << std::endl;
                    } else if (action == "monster") {
                        // Chance for a monster to appear
                        srand(time(0));
                        int randomNum = rand() % 10;
                        if (randomNum < 3) {
                            std::cout << "A monster has appeared!" << std::endl;
                            bool playerWins = PlayRockPaperScissors();
                            if (playerWins) {
                                std::cout << "You defeated the monster!" << std::endl;
                            } else {
                                std::cout << "The monster defeated you!" << std::endl;
                                player.TakeDamage(20);
                            }
                        } else {
                            std::cout << "No monster appeared." << std::endl;
                        }
                    } else {
                        std::cout << "Invalid action. Please try again." << std::endl;
                    }

                    // Display items in the room
                    std::cout << "Items in the room:" << std::endl;
                    for (const Item& item : player.GetLocation()->GetItems()) {
                        std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
                    }

                    // Display player's inventory
                    std::cout << "Player's Inventory:" << std::endl;
                    for (const Item& item : player.GetInventory()) {
                        std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
                    }
                }

                return 0;
            }
